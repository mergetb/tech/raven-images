#install # deprecated
text
keyboard us
lang en_US.UTF-8
skipx
network --device eth0 --bootproto dhcp
rootpw --plaintext test
user --name=rvn --password=rvn
sshkey --username rvn "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC3e0frRD6el0aK+kh6Ba4/Mc3pYs99BBVWf8pw3yMO31wR6+l/uscSu15oJGxbfsZknqZKKVfD925q03wcblzA3z0Q9F6uPb+gK+/q8+xWU9yzDKNF7zjqdzVuP+6E2xQv48pfAxu1/m3bqQHL1jLIbJG2V+53WaaCx+RG8OmyxPH0MVHzX2ajtpDP1K6/fKr+wCefxJ2yo6WRRh/95KlhaxLNPNql3xJno3HIYd3x2+hR0hajtPHM6Gj2LORVUXui8M0ZaO/NWeyy3mcK3KTk2rkKlztuIFUyp7Z5L+wPzp6MEgDaQjoOEZXEM7WwqbeSNYv/2pQlOlZcTyYhRuTR ry@mirror"
sshkey --username root "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIPHUpmFabDiKwhsTzMqWlNGGOcHERO/66eeuhFSTfhUv packer_provisioning"
firewall --disabled
selinux --disabled
timezone --utc America/Los_Angeles 
timesource --ntp-server north-america.pool.ntp.org
services --enabled=sshd
# The biosdevname and ifnames options ensure we get "eth0" as our interface
# even in environments like virtualbox that emulate a real NW card
bootloader --append="console=ttyS0,115200n8 console=tty0 net.ifnames=0 biosdevname=0"
zerombr
clearpart --all --drives=vda
part /boot/efi --fstype=efi --size=100 --label=efi --ondisk=vda
part / --fstype=ext4 --asprimary --size=1024 --grow --label root --ondisk=vda
#part swap --size=1024 --label swap

# Using explicit definitions of "$releasever" 9-stream, "$basearch" x86_64
url --url=http://mirror.stream.centos.org/9-stream/BaseOS/x86_64/os/
# centos-release-hyperscale should be in here. It's actually in 
#  the centos-addons repo in the real os.
repo --name sig-extras --baseurl https://mirror.stream.centos.org/SIGs/9-stream/extras/x86_64/extras-common/
# and we need to include this to actually install systemd-networkd during the kickstart
repo --name sig-hyperscale --baseurl https://mirror.stream.centos.org/SIGs/9-stream/hyperscale/x86_64/packages-main/
# as of mid-2024, hyperscale is on systemd 255, and epel is on systemd 253.

# these are in the real os so lets include them for install
repo --name appstream --baseurl https://mirror.stream.centos.org/9-stream/AppStream/x86_64/os/
repo --name crb --baseurl https://mirror.stream.centos.org/9-stream/CRB/x86_64/os/

#repo --name epel --mirrorlist https://mirrors.fedoraproject.org/mirrorlist?repo=epel-8&arch=$basearch
# epel probably not rqeuired since networkd is in hyperscale version of systemd.

reboot

%packages --inst-langs=en
@core
#epel-release
# includes the lsb_release command
# lsb_release #replaced redhat-lsb-core
bash-completion
man-pages
bzip2
rsync
yum-utils
net-tools
# from raven customizations, previously in dnf.sh
vim-minimal
#emacs-nox # stream9 nothing provides libtree-sitter.so.0 
tree
tmux
nfs-utils
python3-libselinux
# requested package changes to use hyperscale version of
# systemd (includes systemd-networkd) # (no it doesnt)
centos-release-hyperscale
-NetworkManager
-NetworkManager-tui
-NetworkManager-team
-firewalld
systemd-networkd
# experiments usually don't use graphics no need for Plymouth
-plymouth
# Don't build rescue initramfs (unless you need to debug on a node)
-dracut-config-rescue
%end


%post

# update systemd with hyperscale - this works in post
# because the repo has been installed with the 'release' rpm above.
# hyperscale repo also includes a newer grep version :shrug:
#dnf upgrade -y systemd grep # yeah but dont bother doing this
systemctl enable systemd-networkd

# trying to remove this for stream-9
## Fix for https://github.com/CentOS/sig-cloud-instance-build/issues/38
#cat > /etc/sysconfig/network-scripts/ifcfg-eth0 << EOF
#DEVICE="eth0"
#BOOTPROTO="dhcp"
#ONBOOT="yes"
#TYPE="Ethernet"
#PERSISTENT_DHCLIENT="yes"
#EOF

# systemd-networkd requested to be used for eth0.
# this may break if you want "consistently named" ifaces.
cat > /etc/systemd/network/20-wired.network << EOF
[Match]
Name=eth0

[Network]
DHCP=yes
EOF

# sshd: disable DNS checks
sed -i 's/.*UseDNS.*/UseDNS no/' /etc/ssh/sshd_config

# rvn sudoers
cat > /etc/sudoers.d/root-group <<EOF
%root ALL=(ALL) NOPASSWD: ALL
%rvn ALL=(ALL) NOPASSWD: ALL
EOF

# adjust fsck's to zero for /
uuid=$(cat /proc/cmdline | grep -Po 'root=UUID=\K([0-9a-f\-]+)')
tune2fs -c 0 "/dev/disk/by-uuid/$uuid"

# systemd should generate a new machine id during the first boot.
# /etc/machine-id should be empty, but it must exist to prevent
# boot errors (e.g.  systemd-journald failing to start).
[[ -f /etc/machine-id ]] && :>/etc/machine-id
[[ -f /var/lib/dbus/machine-id ]] && :>/var/lib/dbus/machine-id
[[ -f /var/lib/systemd/random-seed ]] && rm -v /var/lib/systemd/random-seed

# Blacklist the floppy module to avoid probing timeouts
echo blacklist floppy > /etc/modprobe.d/nofloppy.conf
#chcon -u system_u -r object_r -t modules_conf_t /etc/modprobe.d/nofloppy.conf

# Customize the initramfs
pushd /etc/dracut.conf.d
# There's no floppy controller, but probing for it generates timeouts
echo 'omit_drivers+=" floppy "' > nofloppy.conf
popd

# The following step is unlikely to be necessary during %post.
# Rerun dracut for the installed kernel (not the running kernel):
#KERNEL_VERSION=$(rpm -q kernel --qf '%{version}-%{release}.%{arch}\n')
#dracut -fv /boot/initramfs-$(uname -r).img $(uname -r)

# Seal for deployment
#rm -rf /etc/ssh/ssh_host_*
#hostnamectl set-hostname localhost.localdomain
#rm -rf /etc/udev/rules.d/70-*

# clean up package stuff
dnf autoremove -y
dnf clean all -y
%end

%pre
if ! grep "fastestmirror=" /etc/dnf/dnf.conf; then echo "fastestmirror=true" >> /etc/dnf/dnf.conf; fi
%end
