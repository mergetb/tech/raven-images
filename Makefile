# =============================================================================
# Raven images build script
# -------------------------
#
#  This makefile builds the currently supported raven base images from
#  upstream OS base images. Packer is used to bring up the base OS image
#  in a QEMU virtual machine and add the raven public key as well as ensuing
#  python is installed for ansible usage.
#
# =============================================================================

.PHONY: all
all: debian ubuntu cumulus

latest: build/fedora/33/fedora-33 \
	debian-11 \
	ubuntu-2004 \
	build/cumulus/4.3/cumulusvx-4.3 \
	build/freebsd/11/freebsd-11 \
	centos8 

.PHONY: fedora
fedora: build/fedora/27/fedora-27 build/fedora/28/fedora-28 build/fedora/33/fedora-33

.PHONY: centos
centos: centos8 centos8-vanilla

.PHONY: debian
.PHONY: debian-11
debian-11: build/debian/11/debian-11

.PHONY: debian-10
debian-10: build/debian/10/debian-10

.PHONY: debian-12
debian-11: build/debian/12/debian-12

.PHONY: ubuntu
.PHONY: ubuntu-latest
ubuntu-latest: build/ubuntu/latest/ubuntu-latest

.PHONY: ubuntu-2004
ubuntu-2004: build/ubuntu/2004/ubuntu-2004

.PHONY: ubuntu-1804
ubuntu-1804: build/ubuntu/1804/ubuntu-1804

.PHONY: ubuntu-1604
ubuntu-1604: build/ubuntu/1604/ubuntu-1604

.PHONY: cumulus
cumulus: \
	build/cumulus/3.5/cumulusvx-3.5 \
	build/cumulus/4.1/cumulusvx-4.1 \
	build/cumulus/4.2/cumulusvx-4.2 \
	build/cumulus/4.3/cumulusvx-4.3 \
	build/cumulus/5.8/cumulusvx-5.8 \
	build/cumulus/3.5-mvrf/cumulusvx-3.5-mvrf \
	#build/cumulus/3.7/cumulusvx-3.7 \
	#build/cumulus/3.7-mvrf/cumulusvx-3.7-mvrf

.PHONY: freebsd
freebsd: \
	build/freebsd/11/freebsd-11 \
	build/freebsdr/11/freebsd-11r

.PHONY: freebsd-11
freebsd: build/freebsd/11/freebsd-11

.PHONY: install
install: fedora-install debian-install ubuntu-install cumulus-install freebsd-install freebsdr-install

###
### Fedora images
###
FEDORA_MIRROR=http://mirrors.kernel.org/fedora/releases
ARCHIVE_FEDORA_MIRROR=https://archives.fedoraproject.org/pub/archive/fedora/linux/releases
F27_BASE=Fedora-Cloud-Base-27-1.6.x86_64.qcow2
F27_URL=${ARCHIVE_FEDORA_MIRROR}/27/CloudImages/x86_64/images/${F27_BASE}
F28_BASE=Fedora-Cloud-Base-28-1.1.x86_64.qcow2
F28_URL=${ARCHIVE_FEDORA_MIRROR}/28/Cloud/x86_64/images/${F28_BASE}
F33_BASE=Fedora-Cloud-Base-33-1.2.x86_64.qcow2
F33_URL=${FEDORA_MIRROR}/33/Cloud/x86_64/images/${F33_BASE}

build/fedora:
	mkdir -p build/fedora

build/fedora/27/fedora-27: fedora-27.json cloud-init/fedora27-config.iso | build/fedora base/${F27_BASE} 
	sudo rm -rf build/fedora/27
	sudo -E ${packer} build ${var} fedora-27.json

build/fedora/28/fedora-28: fedora-28.json cloud-init/fedora27-config.iso | build/fedora base/${F28_BASE} 
	sudo rm -rf build/fedora/28
	sudo -E ${packer} build ${var} fedora-28.json

build/fedora/33/fedora-33: fedora-33.json cloud-init/fedora27-config.iso | build/fedora base/${F33_BASE} 
	sudo rm -rf build/fedora/33
	sudo -E ${packer} build ${var} fedora-33.json

build/fedora/33-netsane/fedora-33-netsane: fedora-33.json cloud-init/fedora27-config.iso | build/fedora base/${F33_BASE} 
	sudo rm -rf build/fedora/33-netsane
	sudo -E ${packer} build ${var} fedora-33-netsane.json

cloud-init/fedora27-config.iso: cloud-init/build-iso-fedora.sh cloud-init/fedora27/user-data cloud-init/fedora27/meta-data
	cd cloud-init; ./build-iso-fedora.sh

base/$(F27_BASE): | base
	wget --directory-prefix base ${F27_URL}

base/$(F28_BASE): | base
	wget --directory-prefix base ${F28_URL}

base/$(F33_BASE): | base
	wget --directory-prefix base ${F33_URL}

.PHONY: fedora-clean
fedora-clean:
	rm -rf cloud-init/*fedora*.iso

.PHONY: fedora-install27
fedora-install27: build/fedora/27/fedora-27
	install $< /var/rvn/img/fedora-27

.PHONY: fedora-install28
fedora-install28: build/fedora/28/fedora-28
	install $< /var/rvn/img/fedora-28

.PHONY: fedora-install33
fedora-install33: build/fedora/33/fedora-33
	install $< /var/rvn/img/fedora-33

###
### CentOS Images
###
.PHONY: build/centos
build/centos:
	[[ -d build/centos ]] || mkdir -p build/centos

.PHONY: clean-centos9stream
clean-centos9stream:
	rm -rfv build/centos/9s

.PHONY: clean-centos9
clean-centos9: clean-centos9stream

.PHONY: clean-centos8stream
clean-centos8stream:
	rm -rfv build/centos/8s

.PHONY: clean-centos8
clean-centos8: clean-centos8stream

.PHONY: clean-centos
clean-centos:
	rm -rfv build/centos

.PHONY: centos9
centos9: centos9stream

.PHONY: centos9stream
centos9stream: build/centos/9s/centos-9stream

.PHONY: centos8
centos8: centos8stream

.PHONY: centos8stream
centos8stream: build/centos/8s/centos-8stream

.PHONY: centos8-vanilla
centos8stream-vanilla: build/centos/8sv/centos-8stream-vanilla

build/centos/9s/centos-9stream:
	${packer} build ${var} centos-9stream.json
#	sudo -E ${packer} build ${var} centos-9stream.json

build/centos/8s/centos-8stream:
	sudo -E ${packer} build ${var} centos-8stream.json

build/centos/8sv/centos-8stream-vanilla:
	sudo -E ${packer} build ${var} centos-8stream-vanilla.json

.PHONY: install-centos9stream
install-centos9stream: build/centos/9s/centos-9stream
	install -o $$USER -g qemu -m 664 $< /var/rvn/img/centos-9stream.qcow2

.PHONY: install-centos9
install-centos9: install-centos9stream

.PHONY: install-centos8stream
install-centos8stream: build/centos/8s/centos-8stream
	install -o $$USER -g qemu -m 664 $< /var/rvn/img/centos-8stream.qcow2

.PHONY: install-centos8
install-centos8: install-centos8stream

.PHONY: install-centos
install-centos: install-centos8 install-centos9

###
### Debian images
###
DEBIAN_MIRROR=http://cdimage.debian.org/debian-cd
STRETCH_BASE=debian-9.3.0-amd64-netinst.iso
STRETCH_URL=${DEBIAN_MIRROR}/current/amd64/iso-cd/${STRETCH_BASE}
BUSTER_BASE=debian-10.11.0-amd64-netinst.iso
BUSTER_URL=https://cdimage.debian.org/cdimage/archive/10.11.0/amd64/iso-cd/${BUSTER_BASE}
BULLSEYE_BASE=debian-11.9.0-amd64-netinst.iso
#BULLSEYE_URL=https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/${BULLSEYE_BASE}
BULLSEYE_URL=https://cdimage.debian.org/mirror/cdimage/archive/11.9.0/amd64/iso-cd/${BULLSEYE_BASE}
SID_BASE=debian-testing-amd64-netinst.iso
SID_URL=https://cdimage.debian.org/cdimage/weekly-builds/amd64/iso-cd/${SID_BASE}

build/debian:
	mkdir -p build/debian

#build/debian/stretch/debian-stretch: debian-stretch.json | build/debian base/${STRETCH_BASE} 
#	rm -rf build/debian/stretch
#	${packer} build ${var} debian-stretch.json

.PHONY: debian-10
debian-10: build/debian/10/debian-10
build/debian/10/debian-10: debian-10.json | build/debian base/${BUSTER_BASE} 
	sudo -E rm -rf build/debian/10
	sudo -E ${packer} build ${var} debian-10.json

.PHONY: debian-11
debian-11: build/debian/11/debian-11
build/debian/11/debian-11: debian-11.json | build/debian base/${BULLSEYE_BASE} 
	sudo -E rm -rf build/debian/11
	sudo -E ${packer} build ${var} debian-11.json

.PHONY: debian-12
debian-12: build/debian/12/debian-12
build/debian/12/debian-12: debian-12.json | build/debian base/${SID_BASE} 
	sudo -E rm -rf build/debian/12
	sudo -E ${packer} build ${var} debian-12.json

.PHONY: debian-kass
debian-kass: build/debian/kass/debian-kass
build/debian/kass/debian-kass: debian-kass.json | build/debian base/${BULLSEYE_BASE} 
	sudo -E rm -rf build/debian/buster-kass
	sudo -E ${packer} build ${var} debian-kass.json

base/$(STRETCH_BASE): | base
	wget --directory-prefix base ${STRETCH_URL}

base/$(BUSTER_BASE): | base
	wget --directory-prefix base ${BUSTER_URL}

base/$(BULLSEYE_BASE): | base
	wget --directory-prefix base ${BULLSEYE_URL}

base/$(SID_BASE): | base
	wget --directory-prefix base ${SID_URL}

.PHONY: debian-clean
debian-clean:

.PHONY: buster 
buster: build/debian/buster/debian-buster

.PHONY: debian-buster-install
debian-buster-install: build/debian/buster/debian-buster
	install $< /var/rvn/img/debian-buster.qcow2

.PHONY: debian-buster-td-install
debian-buster-td-install: build/debian/buster-td/debian-buster-td
	install $< /var/rvn/img/debian-buster-td.qcow2

.PHONY: debian-kass-install
debian-kass-install: build/debian/kass/debian-kass
	install $< /var/rvn/img/debian-kass.qcow2


###
### Ubuntu images
###
# https://cloud-images.ubuntu.com/releases/18.04/release/SHA256SUMS
UBUNTU_MIRROR=https://cloud-images.ubuntu.com
UBUNTU_1604=ubuntu-16.04-server-cloudimg-amd64-disk1.img
UBUNTU_1804=ubuntu-18.04-server-cloudimg-amd64.img
UBUNTU_2004=ubuntu-20.04-server-cloudimg-amd64.img
UBUNTU_LATEST=ubuntu-21.10-server-cloudimg-amd64.img
UBUNTU16_URL=${UBUNTU_MIRROR}/releases/16.04/release/${UBUNTU_1604}
UBUNTU18_URL=${UBUNTU_MIRROR}/releases/18.04/release/${UBUNTU_1804}
UBUNTU20_URL=${UBUNTU_MIRROR}/releases/focal/release/${UBUNTU_2004}
UBUNTU_LATEST_URL=${UBUNTU_MIRROR}/releases/21.10/release/${UBUNTU_LATEST}

build/ubuntu/1604/ubuntu-1604: ubuntu-1604.json cloud-init/ubuntu-config.iso | build/ubuntu base/${UBUNTU_1604} 
	sudo -E rm -rf build/ubuntu/1604
	sudo -E ${packer} build ${var} ubuntu-1604.json

build/ubuntu/1804/ubuntu-1804: ubuntu-1804.json cloud-init/ubuntu-config.iso | build/ubuntu base/${UBUNTU_1804} 
	sudo -E rm -rf build/ubuntu/1804
	sudo -E ${packer} build ${var} ubuntu-1804.json

build/ubuntu/2004/ubuntu-2004: ubuntu-2004.json cloud-init/ubuntu-config.iso | build/ubuntu base/${UBUNTU_2004} 
	sudo -E rm -rf build/ubuntu/2004
	sudo -E ${packer} build ${var} ubuntu-2004.json

build/ubuntu/latest/ubuntu-latest: ubuntu-latest.json cloud-init/ubuntu-config.iso | build/ubuntu base/${UBUNTU_LATEST} 
	sudo -E rm -rf build/ubuntu/latest
	sudo -E ${packer} build ${var} ubuntu-latest.json

cloud-init/ubuntu-config.iso: cloud-init/build-iso-ubuntu.sh cloud-init/ubuntu/user-data cloud-init/ubuntu/meta-data 
	cd cloud-init; ./build-iso-ubuntu.sh

build/ubuntu:
	mkdir -p build/ubuntu

base/$(UBUNTU_1604): | base
	wget --directory-prefix base ${UBUNTU16_URL}

base/$(UBUNTU_1804): | base
	wget --directory-prefix base ${UBUNTU18_URL}

base/$(UBUNTU_2004): | base
	wget --directory-prefix base ${UBUNTU20_URL}

base/$(UBUNTU_LATEST): | base
	wget --directory-prefix base ${UBUNTU_LATEST_URL}

.PHONY: ubuntu-clean
ubuntu-clean:
	rm -rf cloud-init/*ubuntu*.iso

.PHONY: ubuntu-install16
ubuntu-install16: build/ubuntu/1604/ubuntu-1604
	install $< /var/rvn/img/ubuntu-1604

.PHONY: ubuntu-install18
ubuntu-install18: build/ubuntu/1804/ubuntu-1804
	install $< /var/rvn/img/ubuntu-1804

###
### DENT
###

ONIE_URL=https://mirror.deterlab.net/rvn/img/onie-x86-bios
DENT_URL=https://storage.googleapis.com/3p.content.mergetb.dev/DentOS/RELEASE/buster/amd64/DENTOS-main_ONL-OS10_2021-03-13.0653-a026430_AMD64_INSTALLED_INSTALLER
DENT_STRETCH_URL=https://storage.googleapis.com/3p.content.mergetb.dev/DentOS/RELEASE/stretch/amd64/DENTOS-main_ONL-OS9_2021-03-13.0653-a026430_AMD64_INSTALLED_INSTALLER

### Buster

build/dent:
	mkdir -p build/dent

http/dent/onie-installer:
	wget --directory-prefix http/dent -O $@ ${DENT_URL}

base/onie-x86-bios: | base
	wget --directory-prefix base -O $@ ${ONIE_URL}

build/dent/1/dent1: dent1.json base/onie-x86-bios http/dent/onie-installer | build/dent
	sudo -E ${packer} build ${var} dent1.json

### Buster Stretch

build/dent-stretch:
	mkdir -p build/dent-stretch

http/dent-stretch/onie-installer:
	wget --directory-prefix http/dent-stretch -O $@ ${DENT_STRETCH_URL}

build/dent-stretch/1/dent1-stretch: dent1-stretch.json base/onie-x86-bios http/dent-stretch/onie-installer | build/dent-stretch
	sudo -E ${packer} build ${var} dent1-stretch.json

###
### Sonic
###

SONIC_URL=https://sonic-jenkins.westus2.cloudapp.azure.com/job/vs/job/buildimage-vs-image-202012/lastSuccessfulBuild/artifact/target/sonic-vs.img.gz

build/sonic:
	mkdir -p build/sonic

base/sonic-2020.img.gz: | base
	wget --directory-prefix base -O $@ ${SONIC_URL}

base/sonic-2020.img: base/sonic-2020.img.gz
	cd base && gunzip --keep $(notdir $<)

build/sonic/2020/sonic-2020: sonic-2020.json | build/sonic base/sonic-2020.img
	sudo -E ${packer} build ${var} sonic-2020.json

###
### Cumulus images
###
CUMULUS_MIRROR=http://cumulusfiles.s3.amazonaws.com
CUMULUS_BASE=cumulus-linux-3.5.3-vx-amd64.qcow2
CUMULUS_BASE37=cumulus-linux-3.7.3-vx-amd64-qemu.qcow2
CUMULUS_BASE40=cumulus-linux-4.0.0-vx-amd64-qemu.qcow2
CUMULUS_BASE41=cumulus-linux-4.1.1-vx-amd64-qemu.qcow2
CUMULUS_BASE42=cumulus-linux-4.2.0-vx-amd64-qemu.qcow2
CUMULUS_BASE43=cumulus-linux-4.3.0-vx-amd64-qemu.qcow2
CUMULUS_BASE58=cumulus-linux-5.8.0-vx-amd64-qemu.qcow2
CUMULUS_URL=${CUMULUS_MIRROR}/${CUMULUS_BASE}
CUMULUS_URL37=${CUMULUS_MIRROR}/CumulusLinux-3.7.3/${CUMULUS_BASE37}
CUMULUS_URL40=https://d2cd9e7ca6hntp.cloudfront.net/public/CumulusLinux-4.0.0/${CUMULUS_BASE40}
CUMULUS_URL41=https://d2cd9e7ca6hntp.cloudfront.net/public/CumulusLinux-4.1.1/${CUMULUS_BASE41}
CUMULUS_URL42=https://d2cd9e7ca6hntp.cloudfront.net/public/CumulusLinux-4.2.0/${CUMULUS_BASE42}
CUMULUS_URL43=https://d2cd9e7ca6hntp.cloudfront.net/public/CumulusLinux-4.3.0/${CUMULUS_BASE43}
CUMULUS_URL58=https://d2cd9e7ca6hntp.cloudfront.net/public/CumulusLinux-5.8.0/${CUMULUS_BASE58}

build/cumulus:
	mkdir -p build/cumulus

build/cumulus/3.5/cumulusvx-3.5: cumulus-35.json | build/cumulus base/${CUMULUS_BASE} 
	sudo rm -rf build/cumulus/3.5
	sudo -E ${packer} build ${var} cumulus-35.json

build/cumulus/3.5-mvrf/cumulusvx-3.5-mvrf: cumulus-35-mvrf.json | build/cumulus base/${CUMULUS_BASE} 
	sudo rm -rf build/cumulus/3.5-mvrf
	sudo -E ${packer} build ${var} cumulus-35-mvrf.json

build/cumulus/3.7-mvrf/cumulusvx-3.7-mvrf: cumulus-37-mvrf.json | build/cumulus base/${CUMULUS_BASE37} 
	sudo rm -rf build/cumulus/3.7-mvrf
	sudo -E ${packer} build ${var} cumulus-37-mvrf.json

build/cumulus/3.7/cumulusvx-3.7: cumulus-37.json | build/cumulus base/${CUMULUS_BASE37} 
	sudo rm -rf build/cumulus/3.7
	sudo -E ${packer} build ${var} cumulus-37.json

.PHONY: cumulusvx-4.0
cumulusvx-4.0: build/cumulus/4.0/cumulusvx-4.0


build/cumulus/4.0/cumulusvx-4.0: cumulus-40.json | build/cumulus base/${CUMULUS_BASE40} 
	sudo rm -rf build/cumulus/4.0
	sudo -E ${packer} build ${var} cumulus-40.json

.PHONY: cumulusvx-4.1
cumulusvx-4.1: build/cumulus/4.1/cumulusvx-4.1

build/cumulus/4.1/cumulusvx-4.1: cumulus-41.json | build/cumulus base/${CUMULUS_BASE41} 
	sudo rm -rf build/cumulus/4.1
	sudo -E ${packer} build ${var} cumulus-41.json

.PHONY: cumulusvx-4.2
cumulusvx-4.2: build/cumulus/4.2/cumulusvx-4.2

build/cumulus/4.2/cumulusvx-4.2: cumulus-42.json | build/cumulus base/${CUMULUS_BASE42} 
	sudo rm -rf build/cumulus/4.2
	sudo -E ${packer} build ${var} cumulus-42.json

.PHONY: cumulusvx-4.3
cumulusvx-4.3: build/cumulus/4.3/cumulusvx-4.3

build/cumulus/4.3/cumulusvx-4.3: cumulus-43.json | build/cumulus base/${CUMULUS_BASE43} 
	sudo rm -rf build/cumulus/4.3
	sudo -E ${packer} build ${var} cumulus-43.json

.PHONY: cumulusvx-5.8
cumulusvx-5.8: build/cumulus/5.8/cumulusvx-5.8

build/cumulus/5.8/cumulusvx-5.8: cumulus-58.json | build/cumulus base/${CUMULUS_BASE58}
	sudo rm -rf build/cumulus/5.8
	sudo -E ${packer} build ${var} cumulus-58.json


base/$(CUMULUS_BASE): | base
	wget --directory-prefix base ${CUMULUS_URL}

base/$(CUMULUS_BASE37): | base
	wget --directory-prefix base ${CUMULUS_URL37}

base/$(CUMULUS_BASE40): | base
	wget --directory-prefix base ${CUMULUS_URL40}

base/$(CUMULUS_BASE41): | base
	wget --directory-prefix base ${CUMULUS_URL41}

base/$(CUMULUS_BASE42): | base
	wget --directory-prefix base ${CUMULUS_URL42}

base/$(CUMULUS_BASE43): | base
	wget --directory-prefix base ${CUMULUS_URL43}

base/$(CUMULUS_BASE58): | base
	wget --directory-prefix base ${CUMULUS_URL58}

.PHONY: cumulus-install
cumulus-install: \
	build/cumulus/3.5/cumulusvx-3.5 \
	build/cumulus/3.7/cumulusvx-3.7 \
	build/cumulus/3.5-mvrf/cumulusvx-3.5-mvrf \
	build/cumulus/3.7-mvrf/cumulusvx-3.7-mvrf
	install $(word 1,$^) /var/rvn/img/cumulusvx-3.5
	install $(word 1,$^) /var/rvn/img/cumulusvx-3.7
	install $(word 2,$^) /var/rvn/img/cumulusvx-3.5-mvrf
	install $(word 2,$^) /var/rvn/img/cumulusvx-3.7-mvrf


###
### Freebsd Images
###
FREEBSD_MIRROR=http://ftp.freebsd.org
FREEBSD11_BASE=FreeBSD-11.4-RELEASE-amd64-disc1.iso
FREEBSD11_URL=${FREEBSD_MIRROR}/pub/FreeBSD/releases/ISO-IMAGES/11.4/${FREEBSD11_BASE}

# freebsd-11

base/$(FREEBSD11_BASE): | base
	wget --directory-prefix base ${FREEBSD11_URL}

build/freebsd:
	mkdir -p build/freebsd


build/freebsd/11/freebsd-11: freebsd-11.json | build/freebsd base/${FREEBSD11_BASE} 
	sudo rm -rf build/freebsd/11
	sudo -E ${packer} build ${var} freebsd-11.json

.PHONY: freebsd-install
freebsd-install: build/freebsd/11/freebsd-11
	install $< /var/rvn/img/freebsd-11.qcow2

# freebsd-11r

build/freebsdr:
	mkdir -p build/freebsdr

build/freebsdr/11/freebsd-11r: freebsd-11-router.json | base/${FREEBSD11_BASE} build/freebsdr 
	sudo rm -rf build/freebsdr/11
	sudo -E ${packer} build ${var} freebsd-11-router.json

.PHONY: freebsdr-install
freebsdr-install: build/freebsdr/11/freebsd-11r
	install $< /var/rvn/img/freebsd-11r.qcow2

# freebsd-11d

build/freebsdd:
	mkdir -p build/freebsdd

build/freebsdd/11/freebsd-11d: freebsd-11-deter.json | base/${FREEBSD11_BASE} build/freebsdd 
	sudo rm -rf build/freebsdd/11
	sudo -E ${packer} build ${var} freebsd-11-deter.json

.PHONY: freebsdd-install
freebsdd-install: build/freebsdd/11/freebsd-11r
	install $< /var/rvn/img/freebsd-11r.qcow2

###
### Helpers
###

# cracklib comes with an executable called 'packer' that typically goes into
# /usr/sbin, so when executing `packer` on a system with cracklib that is 
# what gets resolved, save the packer path here for later use with sudo
packer := $(if $(ci),/usr/bin/packer,`which packer`)
var := $(if $(ci),-var "headless=true" -var "display=none" -var "date=`date +'%Y%m%d'`",-var "date=`date +'%Y%m%d'`")

base:
	mkdir -p base

.PHONY: clean
clean: fedora-clean debian-clean ubuntu-clean
	rm -rf build

.PHONY: distclean
distclean: clean
	rm -rf base
