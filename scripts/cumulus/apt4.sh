cat >> /etc/apt/sources.list <<EOF

#debian repos
deb http://deb.debian.org/debian buster main
#deb http://deb.debian.org/debian-security/ buster/updates main
EOF

printf 'Package: *\nPin: release a=buster\nPin-Priority: 10\n' > /etc/apt/preferences.d/limit-buster

cat >> /etc/apt/apt.conf <<EOF
Acquire::Check-Valid-Until "false";
EOF

apt update -y
#TODO broken: apt dist-upgrade -y
apt install -y nfs-common
