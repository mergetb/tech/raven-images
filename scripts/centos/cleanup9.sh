#!/bin/bash

set -e
set -x

dnf autoremove -y
dnf clean all -y

# this should be done to keep live machine-id's unique
# echo "clearing machine-id files /etc/machine-id, /var/lib/dbus/machine-id"
# :>/etc/machine-id
# :>/var/lib/dbus/machine-id
# did this during kickstart.

# this should be handled by the systemd-networkd that we installed during KS
# echo "clearing out network manager resolvconf"
# cat > /etc/resolv.conf <<EOF
# nameserver 8.8.8.8
# EOF
