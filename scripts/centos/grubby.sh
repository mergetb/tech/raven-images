#!/bin/bash

set -e

# troubleshooting/debugging output
cat /etc/default/grub
# debug output #2 (using grubby)
grubby --info `grubby --default-kernel`

grubby --update-kernel=ALL --remove-args="rhgb quiet" --args="console=tty0 console=ttyS0,115200n8"

grubby --info `grubby --default-kernel`
